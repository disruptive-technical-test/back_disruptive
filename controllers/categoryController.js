const Category = require('../models/Category');

exports.createCategory = async (req, res) => {
  const { name, permissions, coverImage } = req.body;

  try {
    let category = await Category.findOne({ name });
    if (category) {
      return res.status(400).json({ msg: 'Category already exists' });
    }

    category = new Category({
      name,
      permissions,
      coverImage
    });

    await category.save();
    res.json({ msg: 'Category created successfully', category });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
};

exports.getCategories = async (req, res) => {
  try {
    const categories = await Category.find();
    res.json(categories);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
};

exports.updateCategory = async (req, res) => {
    const { id } = req.params;
    const { name, permissions, coverImage } = req.body;
  
    try {
      let category = await Category.findById(id);
      if (!category) {
        return res.status(404).json({ msg: 'Category not found' });
      }
  
      category.name = name || category.name;
      category.permissions = permissions || category.permissions;
      category.coverImage = coverImage || category.coverImage;
  
      await category.save();
      res.json({ msg: 'Category updated successfully', category });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  };
