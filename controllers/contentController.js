const Content = require('../models/Content');
const Category = require('../models/Category');
const User = require('../models/User');


exports.createContent = async (req, res) => {
    try {
        const { title, type, categoryName, fileUrl, body, imageUrl } = req.body;
        const user = await User.findById(req.user.id).populate('role', 'name');
        const credits = user.username;

        if (!user.role || (user.role.name !== 'Admin' && user.role.name !== 'Creador')) {
            return res.status(403).json({ msg: 'No tienes permisos para crear contenido.' });
        }
      
        const category = await Category.findOne({ $or: [{ categoryName }, { _id: categoryName }] });
        if (!category) {
            return res.status(404).json({ msg: 'Categoría no encontrada' });
        }
       
        const newContent = new Content({
            title,
            type,
            category: category._id,
            credits,
            fileUrl,
            body,
            imageUrl
        });

       
        await newContent.save();

        res.json({ msg: 'Contenido creado exitosamente', content: newContent });
    } catch (err) {
        console.error('Error al crear contenido:', err.message);
        res.status(500).send('Error del servidor');
    }
};


exports.getContent = async (req, res) => {
    try {
        const { id } = req.params; 

        const content = await Content.findById(id).populate('category', 'name');
        if (!content) {
            return res.status(404).json({ msg: 'Contenido no encontrado' });
        }

        res.json(content);
    } catch (err) {
        console.error('Error al obtener contenido:', err.message);
        res.status(500).send('Error del servidor');
    }
};


exports.getContents = async (req, res) => {
    try {
        const { categoryId } = req.params;

        if (!categoryId) {
            return res.status(400).json({ msg: 'ID de categoría es requerido' });
        }

        const contents = await Content.find({ category: categoryId }).populate('category', 'name');

        if (contents.length === 0) {
            return res.status(404).json({ msg: 'No se encontraron contenidos para esta categoría' });
        }

        res.json(contents);
    } catch (err) {
        console.error('Error al obtener contenidos:', err.message);
        res.status(500).send('Error del servidor');
    }
};

exports.updateContent = async (req, res) => {
    const { title, type, categoryName, credits, fileUrl } = req.body;
    const contentFields = { title, type, credits, fileUrl };
    const user = await User.findById(req.user.id).populate('role', 'name');

    try {
        if (!user.role || (user.role.name !== 'Admin' && user.role.name !== 'Creador')) {
            return res.status(403).json({ msg: 'No tienes permisos para actualizar el contenido.' });
        }

        let content = await Content.findById(req.params.id);
        if (!content) {
            return res.status(404).json({ msg: 'Contenido no encontrado' });
        }

        if (categoryName) {
            const category = await Category.findOne({ name: categoryName });
            if (!category) {
                return res.status(404).json({ msg: 'Categoría no encontrada' });
            }
            contentFields.category = category._id;
        }

        content = await Content.findByIdAndUpdate(req.params.id, { $set: contentFields }, { new: true });

        res.json({ msg: 'Contenido actualizado', content });
    } catch (err) {
        console.error('Error al actualizar contenido:', err.message);
        res.status(500).send('Error del servidor');
    }
};

exports.deleteContent = async (req, res) => {
    const user = await User.findById(req.user.id).populate('role', 'name');
    try {
        if (!user.role || user.role.name !== 'Admin') {
            return res.status(403).json({ msg: 'No tienes permisos para eliminar el contenido.' });
        }
        let content = await Content.findById(req.params.id);

        if (!content) {
            return res.status(404).json({ msg: 'Contenido no encontrado' });
        }

        await Content.findByIdAndDelete(req.params.id);

        res.json({ msg: 'Contenido eliminado' });
    } catch (err) {
        console.error('Error al eliminar contenido:', err.message);
        res.status(500).send('Error del servidor');
    }
};
