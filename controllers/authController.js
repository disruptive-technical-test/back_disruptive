const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const Role = require('../models/Role');
const secretKey = process.env.JWT_SECRET;

exports.register = async (req, res) => {
  const { username, email, password, role } = req.body;
  try {

    let user = await User.findOne({ email });
    if (user) {
      return res.status(400).json({ msg: 'El correo electrónico ya está en uso.' });
    }

    user = await User.findOne({ username });
    if (user) {
      return res.status(400).json({ msg: 'El nombre de usuario ya está en uso.' });
    }

    const roleData = await Role.findOne({ name: role });
    console.log(roleData, req.body)

    user = new User({
      username,
      email,
      password,
      role: roleData._id
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();

    const payload = {
      user: {
        id: user.id,
        role: user.role
      }
    };

    jwt.sign(
      payload,
      secretKey,
      { expiresIn: 7200 },
      (err, token) => {
        if (err) throw err;
        res.json({ token });
      }
    );
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
};

exports.login = async (req, res) => {
  const { email, password } = req.body;
  try {
    let user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ msg: 'Credenciales inválidas.' });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({ msg: 'Credenciales inválidas.' });
    }

    const payload = {
      user: {
        id: user.id,
        role: user.role
      }
    };

    jwt.sign(
      payload,
      secretKey,
      { expiresIn: 7200 },
      (err, token) => {
        if (err) throw err;
        res.json({ token });
      }
    );
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};
