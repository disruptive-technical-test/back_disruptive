const mongoose = require('mongoose');

const ContentSchema = new mongoose.Schema({
  title: { type: String, required: true },
  type: { type: String, enum: ['video', 'image', 'text'], required: true },
  category: { type: mongoose.Schema.Types.ObjectId, ref: 'Category', required: true },
  credits: { type: String }, 
  fileUrl: { type: String }, 
  body: { type: String},
  imageUrl: { type: String}
});

module.exports = mongoose.model('Content', ContentSchema);
