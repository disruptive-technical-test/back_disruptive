const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  permissions: {
    images: {
      type: Boolean,
      default: false
    },
    videos: {
      type: Boolean,
      default: false
    },
    texts: {
      type: Boolean,
      default: false
    }
  },
  coverImage: {
    type: String,
    required: false
  }
});

module.exports = mongoose.model('Category', CategorySchema);
