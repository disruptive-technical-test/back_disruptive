const mongoose = require('mongoose');

const ThemeSchema = new mongoose.Schema({
  name: { type: String, unique: true, required: true },
  allowsImages: { type: Boolean, default: false },
  allowsVideos: { type: Boolean, default: false },
  allowsTexts: { type: Boolean, default: false },
});

module.exports = mongoose.model('Theme', ThemeSchema);
