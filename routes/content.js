const express = require('express');
const router = express.Router();
const { createContent, getContent, getContents, updateContent, deleteContent } = require('../controllers/contentController');
const { authMiddleware, adminMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, createContent);
router.get('/category/:categoryId', authMiddleware, getContents);
router.get('/:id', authMiddleware, getContent);
router.put('/:id', authMiddleware, adminMiddleware, updateContent);
router.delete('/:id', authMiddleware, adminMiddleware, deleteContent);

module.exports = router;
