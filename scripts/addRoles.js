const mongoose = require('mongoose');
const Role = require('../models/Role');
const connectDB = require('../config/db');

connectDB();

const getRoles = async () => {
  try {
    const roles = await Role.find({});
    console.log('Roles:', roles);
    process.exit(0);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

getRoles();
